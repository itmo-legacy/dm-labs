#include <sstream>

#include "playground.h"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
3 3
1 2
2 3
1 3
1 2 3
)";
    auto output = R"(
YES
0 0 2 0 4 0 
1 0
3 0
2 6
)";
    interact(input, output);
}

TEST(correctness, n1) {
    auto input  = R"(
1 0
1
)";
    auto output = R"(
YES
0 0
)";
    interact(input, output);
}

TEST(correctness, n2) {
    auto input  = R"(
2 1
1 2
2 1
)";
    auto output = R"(
YES
2 0 0 0 
1 4
)";
    interact(input, output);
}

TEST(correcntess, k4) {
    auto input = R"(
4 6
4 1
4 2
3 4
1 2
1 3
2 3
3 2 4 1
)";
    auto output = R"(
YES
6 0 2 0 0 0 4 0 
5 0
3 0
2 -2
4 2
3 8
1 0
)";
    interact(input, output);
}

TEST(correcntess, k5) {
    auto input = R"(
5 10
1 2
1 3
1 4
1 5
2 3
2 4
2 5
3 4
3 5
4 5
3 2 4 1 5
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correcntess, k3_3) {
    auto input = R"(
6 9
1 4
1 5
1 6
2 4
2 5
2 6
3 4
3 5
3 6
6 1 4 2 5 3
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correcntess, k3_3_and_edge) {
    auto input = R"(
6 10
1 4
1 5
1 6
2 4
2 5
2 6
3 4
3 5
3 6
4 6
6 1 4 2 5 3
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correcntess, n6m10) {
    auto input = R"(
6 10
1 2
2 3
3 4
4 5
5 6
1 3
4 6
2 4
3 5
6 1
1 2 3 4 5 6
)";
    auto output = R"(
YES
0 0 2 0 4 0 6 0 8 0 10 0 
1 0
3 0
5 0
7 0
9 0
2 2
8 -2
4 -2
6 2
5 12
)";
    interact(input, output);
}

TEST(correcntess, complex1) {
    auto input = R"(
6 11
4 1
4 2
3 4
1 2
1 3
2 3
3 6
1 5
5 6
4 6
2 6
3 2 4 1 5 6
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correcntess, complex2) {
    auto input = R"(
6 9
4 1
4 2
3 4
1 2
1 3
2 3
3 6
1 5
5 6
3 2 4 1 5 6
)";
    auto output = R"(
YES
6 0 2 0 0 0 4 0 8 0 10 0 
5 0
3 0
2 -2
4 2
3 3
1 0
5 12
7 0
9 0
)";
    interact(input, output);
}

TEST(correcntess, complex3) {
    auto input = R"(
6 10
4 1
4 2
3 4
1 2
1 3
2 3
3 6
1 5
5 6
4 6
3 2 4 1 5 6
)";
    auto output = R"(
YES
6 0 2 0 0 0 4 0 8 0 10 0 
5 0
3 0
2 2
4 -2
3 -3
1 0
5 12
7 0
9 0
7 3
)";
    interact(input, output);
}

TEST(correcntess, complex4) {
    auto input = R"(
6 10
2 6
4 1
4 2
3 4
1 2
1 3
2 3
3 6
1 5
5 6
3 2 4 1 5 6
)";
    auto output = R"(
YES
6 0 2 0 0 0 4 0 8 0 10 0 
6 -4
5 0
3 0
2 2
4 -2
3 3
1 0
5 12
7 0
9 0
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
