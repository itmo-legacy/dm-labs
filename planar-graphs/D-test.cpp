#include <sstream>

#include "playground.h"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
3
1111111111
000111111011100
111111
)";
    auto output = R"(
NO
NO
YES
)";
    interact(input, output);
}

TEST(correctness, n6_k5) {
    auto input  = R"(
1
111111111010111
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correcntess, n6_no_k5) {
    auto input  = R"(
1
101111111010111
)";
    auto output = R"(
YES
)";
    interact(input, output);
}

TEST(correcntess, k3_3_with_extra_edges1) {
    auto input  = R"(
1
100111111011100
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correcntess, k3_3_with_extra_edges2) {
    auto input  = R"(
1
010111111011101
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correcntess, k6) {
    auto input  = R"(
1
111111111111111
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correctness, empty_string) {
    auto input  = R"(
4
1111111111

000111111011100
111111
)";
    auto output = R"(
NO
YES
NO
YES
)";
    interact(input, output);
}


int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
