#include <algorithm>
#include <cassert>
#include <cstddef>
#include <deque>
#include <iostream>
#include <list>
#include <map>
#include <unordered_set>
#include <vector>

using std::size_t;

template <typename T>
void sort(T &x1, T &x2) {
    if (x1 > x2) {
        std::swap(x1, x2);
    }
}

bool intersect(int l1, int r1, int l2, int r2) {
    assert(r1 >= l1 and r2 >= l2);
    if (r1 > r2) {
        std::swap(l1, l2);
        std::swap(r1, r2);
    }
    return l1 < l2 and r1 > l2 and r1 != r2;
}

struct Point {
    int x, y;
};

struct Edge {
    unsigned a, b;
    size_t i;
    Point p;

    std::vector<Edge *> connections;
    enum Part { UNSET, ONE, TWO } part;

    Edge(unsigned a, unsigned b, size_t i) : a(a), b(b), i(i) {}

    void set_data(Point p) { std::swap(this->p, p); }

    void set_part(Part part) {
        assert(part != UNSET);
        this->part = part;
        p          = {int(a + b), int(i)};
        if (part == TWO) {
            p.y = -p.y;
        }
    }

    void remap(std::vector<size_t> const &mapping) {
        a = mapping[a];
        b = mapping[b];
        sort(a, b);
    }
};

struct EdgePool {
    void add(unsigned a, unsigned b, size_t i) {
        if (a > b) {
            std::swap(a, b);
        }
        unset.emplace_front(a, b, i);
        auto it = unset.begin();
        point_to_edge.emplace(std::make_pair(a, b), it);
    }

    void set_data(unsigned a, unsigned b, Point const &p) {
        if (a > b) {
            std::swap(a, b);
        }
        auto &it = point_to_edge[{a, b}];
        it->set_data(p);
        set.emplace_front(*it);
        unset.erase(it);
        it = set.begin();
    }

    Edge *extract_unset() {
        assert(not unset.empty());
        set.emplace_front(std::move(unset.front()));
        unset.pop_front();
        return &set.front();
    }

    void remap(std::vector<size_t> const &mapping) {
        for (auto &&edge : unset) {
            edge.remap(mapping);
        }
    }

    using iterator = std::list<Edge>::iterator;
    std::map<std::pair<unsigned, unsigned>, iterator> point_to_edge;
    std::list<Edge> unset;
    std::list<Edge> set;
};

bool color(Edge *current, Edge *previous = nullptr) {
    if (not previous) {
        assert(current->part == Edge::UNSET);
        current->part = Edge::ONE;
    } else {
        assert(previous->part != Edge::UNSET);
        if (current->part == Edge::UNSET) {
            current->set_part(previous->part == Edge::ONE ? Edge::TWO : Edge::ONE);
        } else {
            return current->part == previous->part;
        }
    }

    for (auto &&next : current->connections) {
        color(next, current);
    }
};

void solve(std::istream &in, std::ostream &out) {
    EdgePool pool;
    size_t n, m;
    in >> n >> m;
    for (size_t i = 0; i < m; ++i) {
        unsigned a, b;
        in >> a >> b;
        pool.add(a, b, i);
    }
    std::vector<int> xs_of_vs(n);
    std::vector<size_t> mapping(n + 1, -1);
    unsigned first, prev;
    in >> first;
    xs_of_vs[first - 1] = 0 * 2;
    prev                = first;
    mapping[first]      = 0;
    for (size_t i = 1; i < n; ++i) {
        unsigned v;
        in >> v;
        xs_of_vs[v - 1] = i * 2;
        pool.set_data(prev, v, Point{int(i) * 2 - 1, 0});
        mapping[v] = i;
        prev       = v;
    }
    pool.set_data(first, n - 1, Point{int(n), std::numeric_limits<int>::max()});
    pool.remap(mapping);

    std::vector<Edge *> graph;
    while (Edge *edge = pool.extract_unset()) {
        for (auto &&prev_edge : graph) {
            if (intersect(edge->a, edge->b, prev_edge->a, prev_edge->b)) {
                edge->connections.emplace_back(prev_edge);
                prev_edge->connections.emplace_back(edge);
            }
        }
    }

    for (auto &&edge : graph) {
        if (edge->part == Edge::UNSET) {
            if (color(edge)) {
                out << "NO" << std::endl;
                return;
            }
        }
    }

    std::vector<Point> results(m);
    for (auto &&edge : pool.list()) {
        results.at(edge->i) = edge->p;
    }

    out << "YES" << std::endl;
    for (auto &&x : xs_of_vs) {
        out << "0 " << x << " ";
    }
    out << std::endl;
    for (auto &&el : results) {
        out << el.x << " 0.00" << el.y << "\n";
    }
    out << std::endl;
}
