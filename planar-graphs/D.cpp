#include <algorithm>
#include <cassert>
#include <cstddef>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <string>
#include <unordered_set>
#include <vector>

using std::size_t;

int check_bipartite(std::vector<std::vector<int>> const &connections, std::vector<int> &parts,
                    int current, int previous = -1) {
    if (previous == -1) {
        parts[current] = 1;
    } else {
        if (parts[current] != 0) {
            return parts[current] != parts[previous];
        }
        parts[current] = parts[previous] == 1 ? 2 : 1;
    }
    int result = 0;
    for (auto &&next : connections[current]) {
        result += check_bipartite(connections, parts, next, current);
    }
    return result;
}

void solve(std::istream &in, std::ostream &out) {
    unsigned t;
    in >> t;
    std::string dummy;
    std::getline(in, dummy);
    while (t--) {
        std::string m;
        std::getline(in, m);
        if (m.size() == 10 and m.find('0') == std::string::npos) {
            out << "NO\n";
            goto next;
        }
        if (m.size() == 15) {
            std::vector<std::vector<int>> connections(6);
            std::vector<int> parts(6, 0);
            int k = 0;
            for (int i = 0; i < 6; ++i) {
                for (int j = 0; j < i; ++j, ++k) {
                    if (m[k] == '1') {
                        connections[i].push_back(j);
                        connections[j].push_back(i);
                    }
                }
            }

            for (int f = 0; f < 4; ++f) {
                for (int s = f + 1; s < 5; ++s) {
                    for (int t = s + 1; t < 6; ++t) {
                        int colors[6] = {0};
                        colors[f] = colors[s] = colors[t] = 1;
                        auto has_three_diff               = [&colors, &connections](int v) {
                            unsigned diff_count = 0;
                            for (int n : connections[v]) {
                                diff_count += colors[v] != colors[n];
                            }
                            return diff_count >= 3;
                        };
                        bool failed = false;
                        for (int v = 0; v < 6; ++v) {
                            if (not has_three_diff(v)) {
                                failed = true;
                                break;
                            }
                        }
                        if (not failed) {
                            out << "NO\n";
                            goto next;
                        }
                    }
                }
            }

            for (int excluded = 0; excluded < 6; ++excluded) {
                unsigned d3_count = 0;
                bool has_k5       = true;
                for (int v = 0; v < 6 and has_k5; ++v) {
                    if (v == excluded) continue;
                    int degree = connections[v].size();
                    bool has_excluded =
                        std::count(connections[v].begin(), connections[v].end(), excluded) == 1;
                    degree -= has_excluded;
                    if (degree < 3) {
                        has_k5 = false;
                    } else if (degree == 3) {
                        if (not has_excluded) {
                            has_k5 = false;
                        } else if (++d3_count > 2) {
                            has_k5 = false;
                        }
                    }
                }
                if (has_k5) {
                    out << "NO\n";
                    goto next;
                }
            }
        }
        out << "YES\n";
    next:;
    }
}

int main() {
    std::ifstream fin("planaritycheck.in");
    std::ofstream fout("planaritycheck.out");
    solve(fin, fout);
    // solve(std::cin, std::cout);
}
