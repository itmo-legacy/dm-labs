#include <sstream>

#include "playground.h"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
5
0 0 1 0
1 0 1 1
1 1 0 1
0 1 0 0
0 0 1 1
)";
    auto output = R"(
2
0.5
0.5
)";
    interact(input, output);
}


TEST(correctness, different_areas) {
    auto input  = R"(
6
0 0 2 0
2 0 2 2
2 2 0 2
0 2 0 0
0 0 2 2
1 2 2 1
)";
    auto output = R"(
6
0.25
0.25
0.5
0.5
1.75
1.75
)";
    interact(input, output);
}

TEST(correctness, one_line) {
    auto input  = R"(
1
0 0 2 0
)";
    auto output = R"(
0
)";
    interact(input, output);
}

TEST(correctness, parallel_lines) {
    auto input  = R"(
1
0 0 2 0
0 1 2 1
)";
    auto output = R"(
0
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
