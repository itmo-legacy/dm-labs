#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <experimental/optional>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <vector>

using std::size_t;
using std::experimental::optional;

constexpr long double EPS = 1e-19;

int compare(long double d1, long double d2) {
    if (d1 - d2 < -EPS) {
        return -1;
    }
    if (d1 - d2 > EPS) {
        return 1;
    }
    return 0;
}

struct LongDoubleLess {
    bool operator()(long double d1, long double d2) const {
        return compare(d1, d2) < 0;
    }
};

struct Point {
    long double x, y;

    Point(int x, int y) : x(x), y(y) {
        fix_zeroes();
    }
    Point(long double x, long double y) : x(x), y(y) {
        fix_zeroes();
    }

    void fix_zeroes() {
        if (compare(x, 0) == 0) {
            x = +0;
        }
        if (compare(y, 0) == 0) {
            y = +0;
        }
    }

    long double get_angle() const {
        return std::atan2(x, y);
    }

    Point get_normalized() const {
        long double len = std::sqrt(x * x + y * y);
        assert(compare(len, 0) != 0);
        return {x / len, y / len};
    }

    Point operator-() const {
        return {-x, -y};
    }

    Point operator-(Point const &other) const {
        return {x - other.x, y - other.y};
    }

    friend bool operator==(Point const &p1, Point const &p2) {
        return compare(p1.x, p2.x) == 0 and compare(p1.y, p2.y) == 0;
    }

    friend bool operator!=(Point const &p1, Point const &p2) {
        return not(p1 == p2);
    }

    friend bool operator<(Point const &p1, Point const &p2) {
        if (compare(p1.x, p2.x) != 0) {
            return compare(p1.x, p2.x) < 0;
        }
        return compare(p1.y, p2.y) < 0;
    }
};

namespace std {
    template <>
    struct hash<Point> {
        size_t operator()(Point const &p) const {
            hash<long double> hasher;
            return hasher(p.x) ^ hasher(p.y * 1000);
        }
    };
} // namespace std

struct Equation {
    long double a, b, c;
};

Equation get_canonical(Point const &p1, Point const &p2) {
    long double c = p1.x * p2.y - p2.x * p1.y;
    long double a = p2.y - p1.y;
    long double b = p1.x - p2.x;
    return Equation{a, b, c};
}

struct Line {
    Point p1, p2;

    Line(int x1, int y1, int x2, int y2) : p1{x1, y1}, p2{x2, y2} {
        assert(p1 != p2);
    }

    optional<Point> intersect(Line const &other) const {
        Equation e1   = get_canonical(p1, p2);
        Equation e2   = get_canonical(other.p1, other.p2);
        long double d = e1.a * e2.b - e1.b * e2.a;
        if (compare(d, 0) == 0) {
            return {};
        }
        long double x = (e1.c * e2.b - e1.b * e2.c) / d;
        long double y = (e1.a * e2.c - e1.c * e2.a) / d;
        return Point(x, y);
    }
};

long double count_area(std::vector<Point> const &path) {
    assert(path.front() == path.back());
    long double result = 0;
    auto prev          = path.begin();
    for (auto it = std::next(prev); it != path.end(); ++it, ++prev) {
        Point p1 = *prev, p2 = *it;
        result += p1.x * p2.y - p2.x * p1.y;
    }
    return std::abs(result) / 2.0;
}

struct Graph {
    struct Vertex {
        struct PolarLess {
            bool operator()(Point const &p1, Point const &p2) const {
                return compare(p1.get_angle(), p2.get_angle()) < 0;
            }
        };

        Point p;
        std::map<Point, Vertex *, PolarLess> connections;

        Vertex(Point p) : p(p) {}

        void connect_to(Vertex *v) {
            connections.emplace(v->p - this->p, v);
        }

        long double count_face_area() {
            std::vector<Point> path = {p};
            auto prev               = this;
            auto it                 = std::prev(connections.end());
            do {
                auto vector  = it->first;
                auto current = it->second;

                path.emplace_back(current->p);
                prev->connections.erase(it);
                prev = current;
                it   = current->connections.upper_bound(-vector);
                if (it == current->connections.end()) {
                    it = current->connections.begin();
                }
            } while (prev != this);
            return count_area(path);
        }
    };

    std::unordered_map<Point, Vertex *> point_to_vertex;
    std::list<Vertex> vertices;

    void add_arcs(Point const &p1, Point const &p2) {
        if (not point_to_vertex.count(p1)) {
            vertices.emplace_back(p1);
            point_to_vertex.emplace(p1, &vertices.back());
        }
        if (not point_to_vertex.count(p2)) {
            vertices.emplace_back(p2);
            point_to_vertex.emplace(p2, &vertices.back());
        }
        Vertex *v1 = point_to_vertex[p1];
        Vertex *v2 = point_to_vertex[p2];
        v1->connect_to(v2);
        v2->connect_to(v1);
    }
};

constexpr long double LIMIT = 5e-9;

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::vector<Line> lines;
    while (n--) {
        int x1, y1, x2, y2;
        in >> x1 >> y1 >> x2 >> y2;
        lines.emplace_back(x1, y1, x2, y2);
    }

    Graph graph;
    for (auto &&line1 : lines) {
        std::set<Point> intersection_points;
        for (auto &&line2 : lines) {
            optional<Point> pi = line1.intersect(line2);
            if (pi) {
                intersection_points.emplace(*pi);
            }
        }
        if (intersection_points.size() <= 1) {
            continue;
        }
        auto prev = intersection_points.begin();
        for (auto it = std::next(prev); it != intersection_points.end(); ++it, ++prev) {
            graph.add_arcs(*prev, *it);
        }
    }

    std::vector<long double> areas;
    for (auto &&v : graph.vertices) {
        while (not v.connections.empty()) {
            long double area = v.count_face_area();
            if (compare(area, LIMIT) >= 0) {
                areas.emplace_back(area);
            }
        }
    }

    std::sort(areas.begin(), areas.end(), LongDoubleLess{});
    if (areas.size() == 0) {
        out << 0 << std::endl;
        return;
    }
    areas.erase(std::prev(areas.end()));
    out << areas.size() << "\n";
    for (auto &&area : areas) {
        out << std::setprecision(16) << area << "\n";
    }
    out << std::endl;
}

int main() {
    // std::ifstream fin("planaritycheck.in");
    // std::ofstream fout("planaritycheck.out");
    // solve(fin, fout);
    solve(std::cin, std::cout);
}
