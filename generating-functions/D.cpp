#include <utility>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

constexpr size_t LIMIT = 7;
using Count            = unsigned long long;
using Object           = std::array<Count, LIMIT>;

Count exp(Count x, unsigned n) {
    Count result = 1;
    while (n--) {
        result *= x;
    }
    return result;
}

struct Unwinder {
    std::string const expr;
    size_t pos;

    using Handler = Object (Unwinder::*)();

    explicit Unwinder(std::string expression) : expr(std::move(expression)), pos(0) {}

    char sym() {
        if (pos >= expr.size()) {
            throw std::runtime_error("end of input");
        }
        return expr[pos];
    }

    void next() { ++pos; }

    bool accept(char ch) {
        if (sym() == ch) {
            next();
            return true;
        } else {
            return false;
        }
    }

    void expect(char ch) {
        if (sym() != ch) {
            throw std::runtime_error(std::string("unexpected character: ") + ch);
        } else {
            next();
        }
    }

    Object unwind() {
        if (accept('B')) {
            return {0, 1};
        }
        Handler handler = nullptr;
        if (accept('L')) {
            handler = &Unwinder::list_handler;
        } else if (accept('S')) {
            handler = &Unwinder::mset_handler;
        } else if (accept('P')) {
            handler = &Unwinder::pair_handler;
        } else {
            throw std::runtime_error(std::string("unknown character: ") + sym());
        }
        assert(handler);
        expect('(');
        Object outcome = (this->*handler)();
        expect(')');
        return outcome;
    }

    Object list_handler() {
        Object inner   = unwind();
        Object outcome = {1};
        for (size_t total_w = 1; total_w < LIMIT; ++total_w) {
            for (size_t last_w = 1; last_w <= total_w; ++last_w) {
                outcome[total_w] += inner[last_w] * outcome[total_w - last_w];
            }
        }
        return outcome;
    }

    Object mset_handler() {
        Object inner         = unwind();
        Object matrix[LIMIT] = {};
        for (size_t max_w = 0; max_w < LIMIT; ++max_w) {
            matrix[0][max_w] = 1;
        }
        for (size_t total_w = 1; total_w < LIMIT; ++total_w) {
            for (size_t max_w = 1; max_w < LIMIT; ++max_w) {
                matrix[total_w][max_w] = matrix[total_w][max_w - 1];
                Count n                = inner[max_w];
                Count coefficient      = n;
                if (n == 0) {
                    continue;
                }
                for (size_t k = 1; k * max_w <= total_w; ++k) {
                    matrix[total_w][max_w] += coefficient * matrix[total_w - max_w * k][max_w - 1];
                    coefficient *= n + k;
                    coefficient /= k + 1;
                }
            }
        }
        Object outcome;
        for (size_t total_w = 0; total_w < LIMIT; ++total_w) {
            outcome[total_w] = matrix[total_w][LIMIT - 1];
        }
        return outcome;
    }

    Object
    pair_handler() {
        Object inner1 = unwind();
        expect(',');
        Object inner2  = unwind();
        Object outcome = {};
        for (size_t total_w = 0; total_w < LIMIT; ++total_w) {
            for (size_t first_w = 0; first_w <= total_w; ++first_w) {
                outcome[total_w] += inner1[first_w] * inner2[total_w - first_w];
            }
        }
        return outcome;
    }
};

void solve(std::istream &in, std::ostream &out) {
    std::string expression;
    in >> expression;
    Unwinder unwinder(expression);
    Object result = unwinder.unwind();
    for (auto &&el : result) {
        out << el << " ";
    }
    out << std::endl;
}

int main() { solve(std::cin, std::cout); }
