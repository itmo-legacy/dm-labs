#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
P(S(B),L(B))
)";
    auto output = R"(
1 2 3 4 5 6 7 
)";
    interact(input, output);
}

TEST(correctness, example2) {
    auto input  = R"(
S(L(B))
)";
    auto output = R"(
1 1 2 3 5 7 11
)";
    interact(input, output);
}

TEST(correctness, example3) {
    auto input  = R"(
L(P(L(L(L(P(P(P(B,L(B)),L(B)),P(B,L(B)))))),P(B,L(B))))
)";
    auto output = R"(
1 1 2 5 14 42 132
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
S(S(B))
)";
    auto output = R"(
1 1 2 3 5 7 11
)";
    interact(input, output);
}

TEST(correctness, trivial2) {
    auto input  = R"(
S(S(S(B)))
)";
    auto output = R"(
1 1 3 6 14 27 58
)";
    interact(input, output);
}

TEST(correctness, big1) {
    auto input  = R"(
S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(B))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
)";
    auto output = R"(
1 1 66 2211 98021 4026231 170237419
)";
    interact(input, output);
}

TEST(correctness, big2) {
    auto input  = R"(
P(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(B)))))))))))))))))))))))))))))))),S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(S(B)))))))))))))))))))))))))))))))))
)";
    auto output = R"(
1 2 65 1120 24960 510576 10784752
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
