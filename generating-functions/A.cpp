#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

constexpr std::uint64_t TASK_MODULO = 998'244'353;

template <std::uint64_t MODULO>
struct Modular {
    static_assert((MODULO * MODULO / MODULO == MODULO), "MODULO should be less than square root of maximal number");

    constexpr Modular() = default;

    template <typename Integer>
    constexpr Modular(Integer data) : data(std::uint64_t(data) % MODULO) {}

    constexpr Modular operator+(Modular other) const {
        return (data + other.data) % MODULO;
    }

    constexpr Modular operator-(Modular other) const {
        return (data - other.data + MODULO) % MODULO;
    }

    constexpr Modular operator*(Modular other) const {
        return (data * other.data) % MODULO;
    }

    constexpr Modular operator/(Modular other) const {
        return *this * other.inverse();
    }

    constexpr Modular &operator+=(Modular other) {
        *this = *this + other;
        return *this;
    }

    constexpr Modular &operator-=(Modular other) {
        *this = *this - other;
        return *this;
    }

    constexpr Modular inverse() const {
        // n*s + a*t = 1
        // a*t = 1 (mod n)
        std::uint64_t t = 1, r = MODULO,
                      newt = 1, newr = data;
        while (newr != 0) {
            std::uint64_t quotient = r / newr;
            std::tie(t, newt)      = std::make_tuple(newt, t - quotient * newt);
            std::tie(r, newr)      = std::make_tuple(newr, r - quotient * newr);
        }
        if (t < 0) {
            t += MODULO;
        }
        return t;
    }

    constexpr explicit operator std::uint64_t() const {
        return data;
    }

    constexpr bool operator==(Modular other) const {
        return data == other.data;
    }
    std::uint64_t data;
};

template <std::uint64_t MODULO>
std::istream &operator>>(std::istream &istream, Modular<MODULO> &modular) {
    std::uint64_t data;
    istream >> data;
    modular = data;
    return istream;
}

using Polynom = std::vector<Modular<TASK_MODULO>>;

void trim_zeroes(Polynom &poly) {
    auto last_non_zero = std::find_if_not(poly.rbegin(), poly.rend(), [](auto &&el) { return el == 0; });
    poly.erase(last_non_zero.base(), poly.end());
}

size_t get_degree(Polynom const &poly) { return poly.empty() ? 0 : poly.size() - 1; }

auto get(Polynom const &poly, size_t i) { return i < poly.size() ? poly[i] : 0; };

Polynom multiply(Polynom const &a, Polynom const &b) {
    size_t n = a.size(), m = b.size();
    Polynom c(n + m - 1, 0);
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < m; ++j) {
            c[i + j] += a[i] * b[j];
        }
    }
    trim_zeroes(c);
    return c;
}

constexpr size_t DIVISION_LIMIT = 1000;

Polynom inverse(Polynom const &a) {
    Polynom b(2 * DIVISION_LIMIT, 0);
    b[0] = 1;
    for (size_t i = 1; i < b.size(); ++i) {
        for (size_t j = 0; j < i + 1; ++j) {
            b[i] -= get(a, j) * get(b, i - j);
        }
    }
    return b;
}

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;
    ++n, ++m;
    Polynom p(n);
    Polynom q(m);
    for (size_t i = 0; i < n; ++i) {
        in >> p[i];
    }
    for (size_t i = 0; i < m; ++i) {
        in >> q[i];
    }

    Polynom p_plus_q(std::max(n, m));
    for (size_t i = 0; i < p_plus_q.size(); ++i) {
        p_plus_q[i] = get(p, i) + get(q, i);
    }
    trim_zeroes(p_plus_q);
    out << get_degree(p_plus_q) << std::endl;
    for (auto &&el : p_plus_q) {
        out << el.data << " ";
    }
    out << std::endl;

    Polynom p_by_q = multiply(p, q);
    out << get_degree(p_by_q) << std::endl;
    for (auto &&el : p_by_q) {
        out << el.data << " ";
    }
    out << std::endl;

    Polynom inverse_q      = inverse(q);
    Polynom p_divided_by_q = multiply(p, inverse_q);
    for (size_t i = 0; i < DIVISION_LIMIT; ++i) {
        out << get(p_divided_by_q, i).data << " ";
    }
    out << std::endl;
}

int main() { solve(std::cin, std::cout); }
