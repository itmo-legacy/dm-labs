#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

constexpr std::uint64_t TASK_MODULO = 1'000'000'007;

template <std::uint64_t MODULO>
struct Modular {
    static_assert((MODULO * MODULO / MODULO == MODULO), "MODULO should be less than square root of maximal number");

    constexpr Modular() = default;

    template <typename Integer>
    constexpr Modular(Integer data) {
        if (data < 0) {
            std::int64_t remainder = data % std::int64_t(MODULO);
            remainder += MODULO;
            this->data = std::uint64_t(remainder) % MODULO;
        } else {
            this->data = std::uint64_t(data) % MODULO;
        }
    }

    constexpr Modular operator-() const {
        return *this * -1;
    }

    constexpr friend Modular operator+(Modular a, Modular const &b) {
        return a += b;
    }

    constexpr friend Modular operator-(Modular a, Modular const &b) {
        return a -= b;
    }

    constexpr friend Modular operator*(Modular a, Modular const &b) {
        return a *= b;
    }

    constexpr friend Modular operator/(Modular a, Modular const &b) {
        return a /= b;
    }

    constexpr Modular &operator+=(Modular const &other) {
        data = (data + other.data) % MODULO;
        return *this;
    }

    constexpr Modular &operator-=(Modular const &other) {
        return *this += -other;
    }

    constexpr Modular &operator*=(Modular const &other) {
        data = (data * other.data) % MODULO;
        return *this;
    }

    constexpr Modular &operator/=(Modular const &other) {
        return *this *= other.inverse();
    }

    constexpr Modular inverse() const {
        // n*s + a*t = 1
        // a*t = 1 (mod n)
        std::uint64_t t = 0, r = MODULO,
                      newt = 1, newr = data;
        while (newr != 0) {
            Modular quotient  = r / newr;
            std::tie(t, newt) = std::make_tuple(newt, (t - quotient * newt).data);
            std::tie(r, newr) = std::make_tuple(newr, (r - quotient * newr).data);
        }
        assert(t * *this == 1);
        return t;
    }

    constexpr explicit operator std::uint64_t() const {
        return data;
    }

    constexpr bool operator==(Modular const &other) const {
        return data == other.data;
    }

    constexpr bool operator!=(Modular const &other) const {
        return data != other.data;
    }

    std::uint64_t data = 0;
};

template <std::uint64_t MODULO>
std::istream &operator>>(std::istream &istream, Modular<MODULO> &modular) {
    std::uint64_t data;
    istream >> data;
    modular = data;
    return istream;
}

template <std::uint64_t MODULO>
std::ostream &operator<<(std::ostream &ostream, Modular<MODULO> const &modular) {
    return ostream << modular.data;
}

using Count = Modular<TASK_MODULO>;

void solve(std::istream &in, std::ostream &out) {
    size_t k, m;
    in >> k >> m;
    ++m;
    std::set<unsigned> items_set;
    for (size_t i = 0; i < k; ++i) {
        unsigned item;
        in >> item;
        items_set.emplace(item);
    }
    std::vector<unsigned> items(WHOLE(items_set));
    std::vector<Count> trees(m, 0);
    std::vector<Count> tree_pairs(m, 0);
    for (auto &&el : items) {
        trees[el] += 1;
    }
    trees[0] = 1;
    for (size_t total_w = 1; total_w < m; ++total_w) {
        for (auto &&root_w : items) {
            assert(root_w > 0);
            if (root_w > total_w) {
                break;
            }
            trees[total_w] += tree_pairs[total_w - root_w];
        }
        for (size_t first_w = 0; first_w < total_w + 1; ++first_w) {
            size_t second_w = total_w - first_w;
            tree_pairs[total_w] += trees[first_w] * trees[second_w];
        }
    }
    trees.erase(trees.begin());
    for (auto &&el : trees) {
        out << el << " ";
    }
    out << std::endl;
}

int main() { solve(std::cin, std::cout); }
