#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>


void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
4 5
)";
    auto output = R"(
1
1
2
4
8
)";
    interact(input, output);
}

TEST(correctness, example2) {
    auto input  = R"(
7 6
)";
    auto output = R"(
1
1
2
5
14
42
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
2 5
)";
    auto output = R"(
1
0
0
0
0
)";
    interact(input, output);
}

TEST(correctness, big1) {
    auto input  = R"(
5000 5
)";
    auto output = R"(
1
1
2
5
14
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
