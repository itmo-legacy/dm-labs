#include <utility>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

constexpr std::uint64_t TASK_MODULO = 104'857'601;

template <std::uint64_t MODULO>
struct Modular {
    static_assert((MODULO * MODULO / MODULO == MODULO), "MODULO should be less than square root of maximal number");

    constexpr Modular() = default;

    template <typename Integer>
    constexpr Modular(Integer data) {
        if (data < 0) {
            std::int64_t remainder = data % std::int64_t(MODULO);
            remainder += MODULO;
            this->data = std::uint64_t(remainder) % MODULO;
        } else {
            this->data = std::uint64_t(data) % MODULO;
        }
    }

    constexpr Modular operator-() const {
        return *this * -1;
    }

    constexpr friend Modular operator+(Modular a, Modular const &b) {
        return a += b;
    }

    constexpr friend Modular operator-(Modular a, Modular const &b) {
        return a -= b;
    }

    constexpr friend Modular operator*(Modular a, Modular const &b) {
        return a *= b;
    }

    constexpr friend Modular operator/(Modular a, Modular const &b) {
        return a /= b;
    }

    constexpr Modular &operator+=(Modular const &other) {
        data = (data + other.data) % MODULO;
        return *this;
    }

    constexpr Modular &operator-=(Modular const &other) {
        return *this += -other;
    }

    constexpr Modular &operator*=(Modular const &other) {
        data = (data * other.data) % MODULO;
        return *this;
    }

    constexpr Modular &operator/=(Modular const &other) {
        return *this *= other.inverse();
    }

    constexpr Modular inverse() const {
        // n*s + a*t = 1
        // a*t = 1 (mod n)
        std::uint64_t t = 0, r = MODULO,
                      newt = 1, newr = data;
        while (newr != 0) {
            Modular quotient  = r / newr;
            std::tie(t, newt) = std::make_tuple(newt, (t - quotient * newt).data);
            std::tie(r, newr) = std::make_tuple(newr, (r - quotient * newr).data);
        }
        assert(t * *this == 1);
        return t;
    }

    constexpr explicit operator std::uint64_t() const {
        return data;
    }

    constexpr bool operator==(Modular const &other) const {
        return data == other.data;
    }

    constexpr bool operator!=(Modular const &other) const {
        return data != other.data;
    }

    std::uint64_t data = 0;
};

template <std::uint64_t MODULO>
std::istream &operator>>(std::istream &istream, Modular<MODULO> &modular) {
    std::uint64_t data;
    istream >> data;
    modular = data;
    return istream;
}

template <std::uint64_t MODULO>
std::ostream &operator<<(std::ostream &ostream, Modular<MODULO> const &modular) {
    return ostream << modular.data;
}

constexpr size_t DIVISION_LIMIT = 1000;

struct Polynom : private std::vector<Modular<TASK_MODULO>> {
    using base = std::vector<Modular<TASK_MODULO>>;

    using base::value_type;

    using base::base;
    using base::size;
    using base::operator[];
    using base::resize;

    void trim_zeroes() {
        auto last_non_zero = std::find_if_not(rbegin(), rend(), [](auto &&el) { return el == 0; });
        erase(last_non_zero.base(), end());
    }

    size_t degree() const { return empty() ? 0 : size() - 1; }

    value_type get(size_t i) const { return i < size() ? (*this)[i] : 0; }

    Polynom operator-() const {
        Polynom copy = *this;
        for (auto &el : copy) {
            el = -el;
        }
        return copy;
    }

    friend Polynom operator-(Polynom a, Polynom const &b) {
        return a -= b;
    }

    Polynom &operator+=(Polynom const &b) {
        if (size() < b.size()) {
            resize(b.size(), 0);
        }
        for (size_t i = 0; i < size(); ++i) {
            (*this)[i] += b.get(i);
        }
        return *this;
    }

    Polynom &operator-=(Polynom const &b) {
        return *this += -b;
    }

    Polynom &operator*=(Polynom const &b) {
        size_t const n = size(), m = b.size();
        Polynom c(n + m - 1);
        for (size_t i = 0; i < n; ++i) {
            for (size_t j = 0; j < m; ++j) {
                c[i + j] += (*this)[i] * b[j];
            }
        }
        *this = std::move(c);
        return *this;
    }

    Polynom &operator*=(value_type b) {
        for (auto &el : *this) {
            el *= b;
        }
        return *this;
    }

    Polynom &operator/=(value_type b) {
        return *this *= b.inverse();
    }

    Polynom operator*(value_type b) const {
        auto copy = *this;
        return copy *= b;
    }

    Polynom operator/(value_type b) const {
        auto copy = *this;
        return copy /= b;
    }

    Polynom inverse() const {
        Polynom b(2 * DIVISION_LIMIT);
        b[0] = 1;
        for (size_t i = 1; i < b.size(); ++i) {
            for (size_t j = 0; j < i + 1; ++j) {
                b[i] -= get(j) * b.get(i - j);
            }
        }
        return b;
    }

    Polynom negate_arg() const {
        Polynom b = *this;
        for (size_t i = 1; i < b.size(); i += 2) {
            b[i] *= -1;
        }
        return b;
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t k;
    unsigned long long n;
    in >> k >> n;
    --n;
    Polynom seq(2 * k + 10);
    for (size_t i = 0; i < k; ++i) {
        in >> seq[i];
    }
    Polynom c(k + 10);
    for (size_t i = 0; i < k; ++i) {
        in >> c[i + 1];
    }
    Polynom q = Polynom{1} - c;

    while (n >= k) {
        for (size_t i = k; i < 2 * k; ++i) {
            seq[i] = 0;
            for (size_t j = 1; j <= k; ++j) {
                seq[i] += -q[j] * seq[i - j];
            }
        }
        for (size_t i = 0; i < k; ++i) {
            seq[i] = seq[2 * i + n % 2];
        }
        q *= q.negate_arg();
        for (size_t i = 0; i < k + 1; ++i) {
            q[i] = q[i * 2];
        }
        q.resize(k + 1);
        n /= 2;
    }

    out << seq[n] << std::endl;
}

int main() { solve(std::cin, std::cout); }
