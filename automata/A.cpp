#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <numeric>
#include <algorithm>

using std::size_t;

const std::string taskname = "problem1";

void solve(std::istream &in, std::ostream &out) {
    std::string word;
    in >> word;
    size_t n, m, k;
    in >> n >> m >> k;
    std::vector<int> accept_states(k);
    for (auto &item: accept_states) {
        in >> item;
    }
    std::vector<std::vector<int>> table(n + 1, std::vector<int>(26, 0));
    for (std::size_t _ = 0; _ < m; ++_) {
        int a, b;
        char c;
        in >> a >> b >> c;
        table[a][c - 'a'] = b;
    }
    auto transition = [&table](int state, char ch) { return table[state][ch - 'a']; };

    int final_state = std::accumulate(word.begin(), word.end(), 1, transition);

    if (std::find(accept_states.begin(), accept_states.end(), final_state) != accept_states.end()) {
        out << "Accepts\n";
    } else {
        out << "Rejects\n";
    }

}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}
