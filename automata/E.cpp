#include <set>
#include <unordered_map>
#include <string>
#include <vector>
#include <cstring>
#include <unordered_set>
#include <queue>
#include <fstream>
#include <iostream>

using std::size_t;

const std::string taskname = "problem5";

using State = int;

using transition_t = std::vector<std::set<State>>;

constexpr uint32_t MOD = 1'000'000'007;

std::string to_string(std::set<State> const &set) {
    std::string result(set.size(), 0);
    size_t counter = 0;
    for (auto &&item: set) {
        result[counter++] = item;
    }
    return result;
}

void solve(std::istream &in, std::ostream &out) {
    size_t n, m, k, l;
    in >> n >> m >> k >> l;
    std::unordered_set<State> accept_states;
    for (std::size_t i = 0; i < k; ++i) {
        int accept_state;
        in >> accept_state;
        accept_states.emplace(accept_state);
    }
    std::vector<transition_t> table(n + 1, transition_t(26));
    for (std::size_t _ = 0; _ < m; ++_) {
        int a, b;
        char c;
        in >> a >> b >> c;
        table[a][c - 'a'].emplace(b);
    }
    auto transition = [&table](State state, char ch) { return table[state][ch - 'a']; };

    int counter = 1;
    std::queue<State> queue;
    queue.push(1);
#pragma ide diagnostic ignored "CannotResolve"
    std::unordered_map<State, std::set<State>> to_nfa;
    std::unordered_map<std::string, State> to_dfa;
    to_nfa[0] = {};
    to_dfa[""] = 0;
    to_nfa[1] = {1};
    to_dfa["\001"] = 1;

    std::vector<std::vector<State>> dfa_table(2, std::vector<State>(26, 0));
    std::unordered_set<State> dfa_accept_states;
    if (accept_states.count(1)) {
        dfa_accept_states.emplace(1);
    }
    while (not queue.empty()) {
        State top = queue.front();
        queue.pop();
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            std::set<int> new_state;
            for (auto &&old_state: to_nfa[top]) {
                auto children = transition(old_state, ch);
                new_state.insert(children.begin(), children.end());
            }
            auto as_string = to_string(new_state);
            if (not to_dfa.count(as_string)) {
                dfa_table.emplace_back(26, 0);
                to_dfa[as_string] = ++counter;
                to_nfa[counter] = new_state;
                bool accept = false;
                for (auto &&accept_state: accept_states) {
                    if (new_state.count(accept_state)) {
                        accept = true;
                        break;
                    }
                }
                if (accept) { dfa_accept_states.emplace(counter); }
                queue.push(counter);
            }
            dfa_table[top][ch - 'a'] = to_dfa[as_string];
        }
    }
    auto dfa_transition = [&dfa_table](State state, char ch) { return dfa_table[state][ch - 'a']; };

    std::vector<std::vector<int>> dp(l + 1, std::vector<int>(counter + 1, 0));
    dp[0][1] = 1;
    for (std::size_t length = 0; length < l; ++length) {
        for (int state = 1; size_t(state) < counter + 1; ++state) {
            for (char ch = 'a'; ch <= 'z'; ++ch) {
                auto &value = dp[length + 1][dfa_transition(state, ch)];
                value = (value + dp[length][state]) % MOD;
            }
        }
    }
    long long result = 0;
    for (auto &&accept_state: dfa_accept_states) {
        result += dp[l][accept_state];
    }
    out << result % MOD << "\n";
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}
