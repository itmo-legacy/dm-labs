#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <numeric>
#include <algorithm>
#include <unordered_set>

using std::size_t;

const std::string taskname = "problem2";

using transition_t = std::vector<std::unordered_set<int>>;

void solve(std::istream &in, std::ostream &out) {
    std::string word;
    in >> word;
    size_t n, m, k;
    in >> n >> m >> k;
    std::unordered_set<int> accept_states;
    for (std::size_t i = 0; i < k; ++i) {
        int accept_state;
        in >> accept_state;
        accept_states.emplace(accept_state);
    }
    std::vector<transition_t> table(n + 1, transition_t(26, std::unordered_set<int>{0}));
    for (std::size_t _ = 0; _ < m; ++_) {
        int a, b;
        char c;
        in >> a >> b >> c;
        table[a][c - 'a'].emplace(b);
    }
    auto transition = [&table](std::unordered_set<int> const &states, char ch) {
        std::unordered_set<int> result;
        for (auto &&state: states) {
            auto &new_states = table[state][ch - 'a'];
            result.insert(new_states.begin(), new_states.end());
        }
        return result;
    };

    auto final_states = std::accumulate(word.begin(), word.end(), std::unordered_set<int>{1}, transition);

    bool is_accepted = false;
    for (auto &accept_state: accept_states) {
        if (final_states.count(accept_state)) {
            is_accepted = true;
            break;
        }
    }
    if (is_accepted) {
        out << "Accepts\n";
    } else {
        out << "Rejects\n";
    }

}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}
