#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef long double ld;

#define isize(A) int(A.size())

const size_t MAXN = 1e3 + 2;

set<size_t> term;
unordered_map<char, size_t> go[MAXN];
unordered_map<char, vector<size_t> > tgo[MAXN];
vector<bool> used;
bool no_equiv[MAXN][MAXN];
queue<pair<size_t, size_t> > q;

void dfs(size_t v) {
    used[v] = true;

    for (auto i : go[v]) {
        if (!used[i.second]) {
            dfs(i.second);
        }
    }
}

vector<size_t> new_num;
set<pair<pair<size_t, size_t>, char> > edge;
set<size_t> new_term;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    freopen("minimization.in", "r", stdin);
    freopen("minimization.out", "w", stdout);

    size_t n, m, k, start = 1;
    cin >> n >> m >> k;

    ++n;

    for (size_t i = 0; i < k; ++i) {
        size_t t;
        cin >> t;
        term.insert(t);
    }

    for (size_t i = 0; i < m; ++i) {
        size_t a, b;
        char c;
        cin >> a >> b >> c;

        go[a][c] = b;
        tgo[b][c].push_back(a);
    }

    for (size_t i = 1; i < n; ++i) {
        for (char c = 'a'; c <= 'z'; ++c) {
            if (go[i].count(c) == 0) {
                go[i][c] = 0;
                tgo[0][c].push_back(i);
            }
        }
    }

    for (char c = 'a'; c <= 'z'; ++c) {
        go[0][c] = 0;
        tgo[0][c].push_back(0);
    }

    used.resize(n);
    dfs(start);

    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            if ((no_equiv[i][j] == false) && (term.count(i) != term.count(j))) {
                no_equiv[i][j] = no_equiv[j][i] = true;
                q.push({i, j});
            }
        }
    }

    while (!q.empty()) {
        pair<size_t, size_t> cur = q.front();
        q.pop();
        for (char c = 'a'; c <= 'z'; ++c) {
            for (auto i : tgo[cur.first][c]) {
                for (auto j : tgo[cur.second][c]) {
                    if (no_equiv[i][j] == false) {
                        no_equiv[i][j] = true;
                        no_equiv[j][i] = true;
                        q.push({i, j});
                    }
                }
            }
        }
    }

    new_num.resize(n, n);

    for (size_t i = 0; i < n; ++i) {
        if (!no_equiv[0][i]) {
            new_num[i] = 0;
        }
    }

    size_t count_ = 1;
    for (size_t i = 1; i < n; ++i) {
        if (!used[i]) {
            continue;
        }

        if (new_num[i] == n) {
            new_num[i] = count_;

            for (size_t j = i + 1; j < n; ++j) {
                if (!no_equiv[i][j]) {
                    new_num[j] = count_;
                }
            }

            ++count_;
        }
    }

    for (size_t i = 1; i < n; ++i) {
        for (auto j : go[i]) {
            size_t v = new_num[i];
            size_t u = new_num[j.second];
            char c = j.first;

            if (edge.count({{v, u}, c}) == 0 && v != n && u != n && v != 0 && u != 0) {
                edge.insert({{v, u}, c});
            }
        }
    }

    for (size_t i = 0; i < n; ++i) {
        if (term.count(i) && used[i]) {
            new_term.insert(new_num[i]);
        }
    }

    cout << count_ - 1 << " " << edge.size() << " " << new_term.size() << "\n";

    for (size_t i : new_term) {
        cout << i << " ";
    }
    cout << "\n";

    for (auto i : edge) {
        cout << i.first.first << " " << i.first.second << " " << i.second << "\n";
    }

    return 0;
}
