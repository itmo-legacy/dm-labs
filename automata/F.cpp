#include <set>
#include <unordered_map>
#include <string>
#include <vector>
#include <cstring>
#include <unordered_set>
#include <queue>
#include <fstream>
#include <iostream>
#include <functional>

using std::size_t;

const std::string taskname = "isomorphism";

using State = int;

using Graph = std::vector<std::vector<State>>;

bool are_isomorphic(Graph const &g1, std::unordered_set<State> const &accept_states1,
                    Graph const &g2, std::unordered_set<State> const &accept_states2) {
    std::vector<bool> visited(g1.size(), false);

    std::function<bool(State, State)> dfs = [&](State s1, State s2) {
        if (visited[s1]) { return true; }

        if (accept_states1.count(s1) != accept_states2.count(s2)) { return false; }

        visited[s1] = true;

        bool result = true;
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            State child1 = g1[s1][ch - 'a'];
            State child2 = g2[s2][ch - 'a'];
            result &= dfs(child1, child2);
        }
        return result;
    };

    return dfs(1, 1);
}

void solve(std::istream &in, std::ostream &out) {
    size_t n1, m1, k1;
    in >> n1 >> m1 >> k1;
    std::unordered_set<State> accept_states1;
    for (std::size_t _ = 0; _ < k1; ++_) {
        State accept_state;
        in >> accept_state;
        accept_states1.emplace(accept_state);
    }
    Graph graph1(n1 + 1, std::vector<State>(26));
    for (std::size_t _ = 0; _ < m1; ++_) {
        State a, b;
        char c;
        in >> a >> b >> c;
        graph1[a][c - 'a'] = b;
    }
    size_t n2, m2, k2;
    in >> n2 >> m2 >> k2;
    std::unordered_set<State> accept_states2;
    for (std::size_t _ = 0; _ < k2; ++_) {
        State accept_state;
        in >> accept_state;
        accept_states2.emplace(accept_state);
    }
    Graph graph2(n2 + 1, std::vector<State>(26));
    for (std::size_t _ = 0; _ < m2; ++_) {
        State a, b;
        char c;
        in >> a >> b >> c;
        graph2[a][c - 'a'] = b;
    }
    size_t n = std::max(n1, n2) + 1;
    graph1.resize(n, std::vector<State>(26));
    graph2.resize(n, std::vector<State>(26));

    if (are_isomorphic(graph1, accept_states1, graph2, accept_states2)) {
        out << "YES\n";
    } else {
        out << "NO\n";
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}
