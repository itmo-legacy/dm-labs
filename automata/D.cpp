#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <numeric>
#include <algorithm>
#include <unordered_set>
#include <functional>
#include <queue>

using std::size_t;

const std::string taskname = "problem4";

using State = int;

using transition_t = std::vector<std::unordered_set<State>>;

constexpr uint32_t MOD = 1'000'000'007;

void solve(std::istream &in, std::ostream &out) {
    size_t n, m, k, l;
    in >> n >> m >> k >> l;
    std::unordered_set<State> accept_states;
    for (std::size_t i = 0; i < k; ++i) {
        int accept_state;
        in >> accept_state;
        accept_states.emplace(accept_state);
    }
    std::vector<std::vector<State>> table(n + 1, std::vector<State>(26, 0));
    std::vector<transition_t> reverse_table(n + 1, transition_t(26));
    for (std::size_t _ = 0; _ < m; ++_) {
        int a, b;
        char c;
        in >> a >> b >> c;
        table[a][c - 'a'] = b;
        reverse_table[b][c - 'a'].emplace(a);
    }
    auto transition = [&table](State state, char ch) { return table[state][ch - 'a']; };

    std::vector<std::vector<int>> dp(l + 1, std::vector<int>(n + 1, 0));
    dp[0][1] = 1;
    for (std::size_t length = 0; length < l; ++length) {
        for (int state = 1; size_t(state) < n + 1; ++state) {
            for (char ch = 'a'; ch <= 'z'; ++ch) {
                auto &value = dp[length + 1][transition(state, ch)];
                value = (value + dp[length][state]) % MOD;
            }
        }
    }
    long long result = 0;
    for (auto &&accept_state: accept_states) {
        result += dp[l][accept_state];
    }
    out << result % MOD << "\n";
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}
