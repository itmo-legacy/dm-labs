#include <iostream>
#include <vector>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <fstream>

//#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef long double ld;

#define isize(A) int(A.size())

const size_t MAXN = 5e4 + 5;
const size_t MAX_ALF = 30;
const size_t ALF = 26;

vector<bool> term;
vector<pair<size_t, size_t> > go[MAXN];
vector<size_t> tgo[MAXN][MAX_ALF];
vector<bool> used, rused;
queue<pair<size_t, size_t> > q;

void dfs(size_t v) {
    used[v] = true;

    for (auto i : go[v]) {
        if (!used[i.first]) {
            dfs(i.first);
        }
    }
}

void reverse_dfs(size_t v) {
    rused[v] = true;

    for (size_t c = 0; c < ALF; ++c) {
        for (auto i : tgo[v][c]) {
            if (!rused[i]) {
                reverse_dfs(i);
            }
        }
    }
}

vector<unordered_set<size_t> > splitting;
vector<size_t> class_number;

set<pair<pair<size_t, size_t> , char> > new_go;
unordered_set<size_t> new_term;
vector<size_t> color;
size_t col = 0;

void new_dfs(size_t v) {
    used[v] = true;

    if (color[class_number[v]] == 0) {
        ++col;
        color[class_number[v]] = col;
    }

    for (auto i : go[v]) {
        if (rused[i.first] && !used[i.first]) {
            new_dfs(i.first);
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    freopen("fastminimization.in", "r", stdin);
    freopen("fastminimization.out", "w", stdout);

    size_t n, m, k, start = 1;
    cin >> n >> m >> k;

    ++n;

    term.resize(n);
    for (size_t i = 0; i < k; ++i) {
        size_t t;
        cin >> t;
        term[t] = true;
    }

    for (size_t i = 0; i < m; ++i) {
        size_t a, b;
        char c;
        cin >> a >> b >> c;

        go[a].push_back({b, c - 'a'});
        tgo[b][c - 'a'].push_back(a);
    }

    used.resize(n);
    dfs(start);

    rused.resize(n);
    for (size_t i = 0; i < n; ++i) {
        if (term[i]) {
            reverse_dfs(i);
        }
    }

    splitting.resize(1);
    class_number.resize(n);

    for (size_t i = 1; i < n; ++i) {
        if (used[i] == false || rused[i] == false) {
            continue;
        }

        if (term[i]) {
            splitting[0].insert(i);
            class_number[i] = 0;
        } else {
            if (splitting.size() == 1) {
                splitting.resize(2);
            }

            splitting[1].insert(i);
            class_number[i] = 1;
        }
    }

    for (size_t j = 0; j < splitting.size(); ++j) {
        for (size_t i = 0; i < ALF; ++i) {
            q.push({j, i});
        }
    }

    while (!q.empty()) {
        pair<size_t, size_t> top = q.front();
        q.pop();
        size_t cur = top.first;
        size_t symbol = top.second;

        unordered_map<size_t, vector<size_t> > involved;
        involved.clear();

        for (auto a : splitting[cur]) {
            for (auto r : tgo[a][symbol]) {
                if (!used[r] || !rused[r]) {
                    continue;
                }

                size_t i = class_number[r];
                involved[i].push_back(r);
            }
        }

        for (auto i : involved) {
            if (i.second.size() < splitting[i.first].size()) {
                size_t new_num = splitting.size();
                splitting.resize(new_num + 1);

                for (auto r : i.second) {
                    splitting[i.first].erase(r);
                    splitting[new_num].insert(r);
                }

                if (splitting[new_num].size() > splitting[i.first].size()) {
                    swap(splitting[i.first], splitting[new_num]);
                }

                for (auto r : splitting[new_num]) {
                    class_number[r] = new_num;
                }

                for (size_t c = 0; c < ALF; ++c) {
                    q.push({new_num, c});
                }
            }
        }
    }

    color.resize(n);
    fill(used.begin(), used.end(), false);
    new_dfs(start);

    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < go[i].size(); ++j) {
            if (used[i] && used[go[i][j].first] &&
              rused[i] && rused[go[i][j].first]) {
                size_t a = color[class_number[i]];
                size_t b = color[class_number[go[i][j].first]];
                char c = char(go[i][j].second + 'a');

                new_go.insert({{a, b}, c});
            }
        }
    }

    for (size_t i = 0; i < n; ++i) {
        if (term[i] && used[i]) {
            new_term.insert(color[class_number[i]]);
        }
    }

    if (new_go.size() == 0) {
        col = 0;
    }

    cout << col << " " << new_go.size() << " " << new_term.size() << "\n";

    for (auto i : new_term) {
        cout << i << " ";
    }
    cout << "\n";

    for (auto i : new_go) {
        cout << i.first.first << " " << i.first.second << " " << i.second << "\n";
    }

    return 0;
}
