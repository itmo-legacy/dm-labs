#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <numeric>
#include <algorithm>
#include <unordered_set>
#include <functional>
#include <queue>

using std::size_t;

const std::string taskname = "problem3";

struct State {
    int value;

    State(int value) : value(value) {};

    operator int() const {
        return value;
    }
};

using transition_t = std::vector<std::unordered_set<State>>;

constexpr uint32_t MOD = 1'000'000'007;

namespace std {
    template<>
    struct hash<State> {
        size_t operator()(State const &state) const {
            return size_t(state.value);
        }
    };
}

size_t n, m, k;

template<typename T>
std::vector<State> top_sort(T &transition) {
    std::vector<bool> visited(n + 1, false);
    std::vector<State> result;
    std::function<void(State)> dfs = [&](State state) {
        if (visited[state]) { return; }
        visited[state] = true;
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            dfs(transition(state, ch));
        }
        result.emplace_back(state);
    };
    dfs(1);
    std::reverse(result.begin(), result.end());
    return result;
}

void solve(std::istream &in, std::ostream &out) {
    in >> n >> m >> k;
    std::unordered_set<State> accept_states;
    for (std::size_t i = 0; i < k; ++i) {
        int accept_state;
        in >> accept_state;
        accept_states.emplace(accept_state);
    }
    std::vector<std::vector<State>> table(n + 1, std::vector<State>(26, 0));
    std::vector<transition_t> reverse_table(n + 1, transition_t(26));
    for (std::size_t _ = 0; _ < m; ++_) {
        int a, b;
        char c;
        in >> a >> b >> c;
        table[a][c - 'a'] = b;
        reverse_table[b][c - 'a'].emplace(a);
    }
    auto transition = [&table](State state, char ch) { return table[state][ch - 'a']; };
    auto reverse_transition = [&reverse_table](State const &state, char ch) { return reverse_table[state][ch - 'a']; };

    std::vector<bool> important(n + 1, false);
    std::function<void(State)> mark_important = [&](State state) {
        static std::vector<bool> visited(n + 1, false);
        if (visited[state]) { return; }
        visited[state] = true;
        important[state] = true;
        for (auto ch = 'a'; ch <= 'z'; ++ch) {
            for (auto &child: reverse_transition(state, ch)) {
                if (not(state == 1 and child != 1)) { mark_important(child); }
            }
        }
    };

    for (auto accept_state: accept_states) {
        mark_important(accept_state);
    }

    std::function<bool(State)> in_cycle = [&](State state) -> bool {
        static std::vector<bool> on_path(n + 1, false);
        static std::vector<bool> visited(n + 1, false);
        if (on_path[state]) { return important[state]; }
        if (visited[state]) { return false; }
        on_path[state] = true;
        visited[state] = true;
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            if (in_cycle(transition(state, ch))) { return true; }
        }
        on_path[state] = false;
        return false;
    };

    if (in_cycle(1)) {
        out << "-1\n";
    } else {
        std::vector<State> sorted = top_sort(transition);
        std::vector<long long> paths(n + 1, 0);
        paths[1] = 1;
        for (auto &&state: sorted) {
            for (char ch = 'a'; ch <= 'z'; ++ch) {
                auto child = transition(state, ch);
                paths[child] = (paths[child] + paths[state]) % MOD;
            }
        }

        long long result = 0;
        for (auto &&accept_state: accept_states) {
            result += paths[accept_state];
        }
        out << result % MOD << "\n";
    }

}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}
