#include <algorithm>
#include <cassert>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <set>
#include <unordered_map>
#include <vector>

using std::size_t;

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::vector<std::pair<unsigned, unsigned>> timeline(n + 1);
    for (size_t i = 0; i < n; ++i) {
        in >> timeline[i].first >> timeline[i].second;
    }
    timeline[n] = {0, 0};
    std::sort(timeline.begin(), timeline.end(), std::greater<>());
    std::multiset<unsigned, std::greater<>> selection;
    auto it             = timeline.begin();
    unsigned prev_limit = it->first;
    while (it != timeline.end()) {
        unsigned current_limit = it->first;
        unsigned erase_count   = std::min(unsigned(selection.size()), prev_limit - current_limit);
        selection.erase(selection.begin(), std::next(selection.begin(), erase_count));
        prev_limit = current_limit;
        while (it != timeline.end() and it->first == prev_limit) {
            selection.emplace(it->second);
            ++it;
        }
    }

    unsigned long long result = std::accumulate(selection.begin(), selection.end(), 0ULL);
    out << result << std::endl;
}

int main() {
    std::ifstream fin("schedule.in");
    std::ofstream fout("schedule.out");
    solve(fin, fout);
    // solve(std::cin, std::cout);
}
