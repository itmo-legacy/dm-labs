#include <sstream>

#include "playground.h"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
2
1 1
1 2
)";
    auto output = R"(
1
)";
    interact(input, output);
}

TEST(correctness, curious1) {
    auto input  = R"(
5
0 5
0 6
1 3
2 4
2 5
)";
    auto output = R"(
14
)";
    interact(input, output);
}

TEST(correctness, big1) {
    auto input  = R"(
6
189 18
12 28
2 38
4 32
4 89
13 3243
)";
    auto output = R"(
0
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
