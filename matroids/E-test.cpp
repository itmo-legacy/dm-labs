#include <sstream>

#include "playground.h"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
3 1
10 20 30
3 1 3 2
)";
    auto output = R"(
50
)";
    interact(input, output);
}

TEST(correctness, trivial) {
    auto input  = R"(
3 3
1 2 3
2 1 2
2 2 3
2 1 3
)";
    auto output = R"(
3
)";
    interact(input, output);
}

TEST(correctness, curious1) {
    auto input  = R"(
7 5
1 5 10 7 12 16 18
3 1 2 3
3 1 4 5
3 1 6 7
4 6 7 4 5
4 7 6 2 3
)";
    auto output = R"(
56
)";
    interact(input, output);
}

TEST(correctness, curious2) {
    auto input  = R"(
5 3
6 5 5 3 4
3 2 3 5
3 1 5 4
4 1 2 3 4
)";
    auto output = R"(
16
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
