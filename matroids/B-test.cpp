#include <sstream>

#include "playground.h"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
6 7 10
1 2 3
1 3 3
2 3 3
3 4 1
4 5 5
5 6 4
4 6 5
)";
    auto output = R"(
2
3 6
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
2 1 5
1 2 3
)";
    auto output = R"(
0
)";
    interact(input, output);
}

TEST(correctness, trivial2) {
    auto input  = R"(
3 3 1
1 2 3
2 3 4
3 1 2
)";
    auto output = R"(
0
)";
    interact(input, output);
}


TEST(correctness, random) {
    auto input  = R"(
6 9 10
1 2 3
1 3 3
2 3 3
3 4 1
4 5 5
5 6 4
4 6 5
6 3 2
1 5 7
)";
    auto output = R"(
4
3 4 6 8
)";
    interact(input, output);
}


int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
