#include <sstream>

#include "playground.h"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
4
1 3 2 4
4 1 2 3 4
2 1 4
2 1 4
2 1 4
)";
    auto output = R"(
2 1 0 4
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
2
5 9
1 1
2 1 2
)";
    auto output = R"(
1 2
)";
    interact(input, output);
}

TEST(correctness, trivial2) {
    auto input  = R"(
2
5 9
1 2
0
)";
    auto output = R"(
2 0
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
