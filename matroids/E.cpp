#ifdef LOCAL
#include "playground.h"
#endif

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <set>
#include <sstream>
#include <vector>

#define WHOLE(container) std::begin(container), std::end(container)

using std::size_t;

std::string const taskname = "cycles";

struct Element {
    size_t index;
    unsigned weight;

    bool operator==(Element const &other) const {
        return index == other.index;
    }

    bool operator<(Element const &other) const {
        if (weight != other.weight) {
            return weight < other.weight;
        }
        return index < other.index;
    }
};

struct Subset {
    Subset() = default;

    void add(Element *el) {
        elements.emplace(el);
    }

    size_t size() const {
        return elements.size();
    }

    bool contains(Subset const &other) const {
        for (auto &&el : other.elements) {
            if (not elements.count(el)) {
                return false;
            }
        }
        return true;
    }

    void remove(Element *el) {
        elements.erase(el);
    }

    Element *smallest() const {
        return *elements.begin();
    }

    std::uint64_t weight() const {
        std::uint64_t result = 0;
        for (auto &&el : elements) {
            result += el->weight;
        }
        return result;
    }

    struct cmp {
        bool operator()(Element const *p1, Element const *p2) const {
            return *p1 < *p2;
        }
    };
    std::set<Element *, cmp> elements;
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;

    std::vector<Element> elements(n);
    for (size_t i = 0; i < n; ++i) {
        in >> elements[i].weight;
        elements[i].index = i + 1;
    }

    std::vector<Subset> cycles(m);
    for (auto &cycle : cycles) {
        size_t elems_count;
        in >> elems_count;
        while (elems_count--) {
            size_t el_i;
            in >> el_i;
            cycle.add(&elements[el_i - 1]);
        }
    }

    Subset base;
    for (auto &el : elements) {
        base.add(&el);
    }

    for (auto const &cycle : cycles) {
        if (base.contains(cycle)) {
            base.remove(cycle.smallest());
        }
    }

    out << base.weight() << std::endl;
}

int main() {
    std::ifstream in(taskname + ".in");
    std::ofstream out(taskname + ".out");
    solve(in, out);
    // solve(std::cin, std::cout);
}
