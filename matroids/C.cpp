#ifdef LOCAL

#include "playground.h"

#endif

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <sstream>
#include <unordered_map>
#include <vector>

#define WHOLE(container) std::begin(container), std::end(container)

using std::size_t;

struct Vertex {
    size_t index;
    unsigned weight = 0;
    std::vector<Vertex *> connected;
    bool used     = false;
    Vertex *match = nullptr;

    void connect_to(Vertex *other) {
        assert(other);
        this->connected.emplace_back(other);
        other->connected.emplace_back(this);
    }

    bool find_augmenting_path() {
        if (used) {
            return false;
        }

        used = true;
        for (auto &&next : connected) {
            if (next->match == nullptr or next->match->find_augmenting_path()) {
                next->match = this;
                this->match = next;
                return true;
            }
        }
        return false;
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::vector<Vertex> vertices(2 * n);
    for (size_t i = 0; i < n; ++i) {
        vertices[i].index     = i;
        vertices[i + n].index = i + n;
        in >> vertices[i].weight;
    }
    for (size_t i = 0; i < n; ++i) {
        size_t edges_count;
        in >> edges_count;
        for (size_t j = 0; j < edges_count; ++j) {
            size_t connected_index;
            in >> connected_index;
            vertices[i].connect_to(&vertices[connected_index + n - 1]);
        }
    }

    std::vector<Vertex *> vertices_view(n);
    for (size_t i = 0; i < n; ++i) {
        vertices_view[i] = &vertices[i];
    }
    std::sort(WHOLE(vertices_view), [](auto v1, auto v2) { return v1->weight > v2->weight; });
    for (auto &&pv : vertices_view) {
        for (auto &v : vertices) {
            v.used = false;
        }
        pv->find_augmenting_path();
    }

    for (size_t i = 0; i < n; ++i) {
        Vertex const &v = vertices[i];
        if (v.match) {
            out << v.match->index % n + 1 << " ";
        } else {
            out << "0 ";
        }
    }
    out << std::endl;
}

int main() {
    std::ifstream in("matching.in");
    std::ofstream out("matching.out");
    solve(in, out);
    // solve(std::cin, std::cout);
}
