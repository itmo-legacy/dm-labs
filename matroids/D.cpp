#ifdef LOCAL

#include "playground.h"

#endif

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <sstream>
#include <unordered_set>
#include <vector>

#define WHOLE(container) std::begin(container), std::end(container)

using std::size_t;

std::string const taskname = "check";

struct Subset {
    Subset() = default;

    template <typename It>
    Subset(It first, It last) : elements(first, last) {}

    void add(unsigned element) {
        elements.push_back(element);
    }

    void fix() {
        std::sort(WHOLE(elements));
    }

    size_t size() const {
        return elements.size();
    }

    bool has_hereditary_property(std::unordered_set<Subset> &present);

    bool can_extend_from(Subset const &other, std::unordered_set<Subset> const &present) const;

    bool operator==(Subset const &other) const {
        return std::equal(WHOLE(elements), WHOLE(other.elements));
    }

    bool operator<(Subset const &other) const {
        return size() < other.size();
    }

    std::vector<unsigned> elements;
};

template <>
struct std::hash<Subset> {
    size_t operator()(Subset const &s) const {
        size_t seed = s.size();
        hash<unsigned> hasher;
        for (auto &&el : s.elements) {
            seed ^= hasher(el) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }
        return seed;
    }
};

bool Subset::has_hereditary_property(std::unordered_set<Subset> &present) {
    for (auto it = elements.begin(); it != elements.end(); ++it) {
        Subset smaller{elements.begin(), it};
        smaller.elements.insert(smaller.elements.end(), std::next(it), elements.end());
        if (not present.count(smaller)) {
            return false;
        }
    }
    present.emplace(*this);
    return true;
}

bool Subset::can_extend_from(Subset const &other, std::unordered_set<Subset> const &present) const {
    for (auto &&el : other.elements) {
        Subset bigger = *this;
        bigger.add(el);
        bigger.fix();
        if (present.count(bigger)) {
            return true;
        }
    }
    return false;
}

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;

    std::vector<Subset> subsets(m);
    for (auto &subset : subsets) {
        size_t elems_count;
        in >> elems_count;
        while (elems_count--) {
            unsigned element;
            in >> element;
            subset.add(element);
        }
        subset.fix();
    }

    std::sort(WHOLE(subsets));
    if (subsets.empty()) {
        out << "NO" << std::endl;
        return;
    }

    std::unordered_set<Subset> present;
    for (auto &&subset : subsets) {
        if (not subset.has_hereditary_property(present)) {
            out << "NO" << std::endl;
            return;
        }
    }

    auto it   = subsets.begin();
    auto next = it;
    while (next != subsets.end() and next->size() == it->size()) {
        ++next;
    }
    while (next != subsets.end()) {
        auto milestone = next;
        while (milestone != subsets.end() and milestone->size() == next->size()) {
            ++milestone;
        }
        while (it != next) {
            for (auto bigger = next; bigger != milestone; ++bigger) {
                if (not it->can_extend_from(*bigger, present)) {
                    out << "NO" << std::endl;
                    return;
                }
            }
            ++it;
        }
        next = milestone;
    }

    out << "YES" << std::endl;
}

int main() {
    std::ifstream in(taskname + ".in");
    std::ofstream out(taskname + ".out");
    solve(in, out);
    // solve(std::cin, std::cout);
}
