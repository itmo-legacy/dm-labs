#ifdef LOCAL
#include "playground.h"
#endif

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>
#include <unordered_map>
#include <vector>

using std::size_t;

enum class State { UNREACHED,
                   IN };

struct Vertex {
    size_t index;

    bool operator==(Vertex const &other) const { return index == other.index; }
    bool operator!=(Vertex const &other) const { return not operator==(other); }
};

struct Edge {
    Vertex *u, *v;
    std::uint64_t w;
    unsigned index;

    Edge(Vertex *u, Vertex *v, std::uint64_t w, unsigned index)
        : u(std::min(u, v)), v(std::max(u, v)), w(w), index(index) {
        assert(u);
        assert(v);
        // assert(u != v); // there might be loops
    }

    friend bool operator==(Edge const &a, Edge const &b) {
        return a.u == b.u and a.v == b.v and a.w == b.w;
    }

    bool operator<(Edge const &other) const { return w < other.w; }

    bool operator>(Edge const &other) const { return other < *this; }
};

struct Dsu {
    bool connected(Vertex const *u, Vertex const *v) {
        return find(u) == find(v);
    }

    void connect(Vertex const *u, Vertex const *v) {
        auto u_leader = find(u);
        auto v_leader = find(v);
        if (u_leader == v_leader) {
            return;
        }

        if (ranks[u_leader] < ranks[v_leader]) {
            std::swap(u_leader, v_leader);
        }

        leaders[v_leader] = u_leader;
        if (ranks[u_leader] == ranks[v_leader]) {
            ++ranks[u_leader];
        }
    }

    Vertex const *find(Vertex const *u) {
        if (not leaders[u] or *leaders[u] == *u) {
            return u;
        }
        assert(leaders[u]);
        leaders[u] = find(leaders[u]);
        return leaders[u];
    }

    std::unordered_map<Vertex const *, Vertex const *> leaders;
    std::unordered_map<Vertex const *, unsigned> ranks;
};

void remove_mst(std::list<Edge> &edges) {
    Dsu dsu;
    auto it = edges.begin();
    while (it != edges.end()) {
        auto const &edge = *it;
        if (not dsu.connected(edge.u, edge.v)) {
            dsu.connect(edge.u, edge.v);
            it = edges.erase(it);
        } else {
            ++it;
        }
    }
}

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    std::uint64_t s;
    in >> n >> m >> s;
    std::vector<Vertex> vertices(n);
    for (size_t i = 0; i < n; ++i) {
        vertices[i].index = i + 1;
    }
    std::vector<Edge> edges;
    for (size_t i = 0; i < m; ++i) {
        unsigned u, v;
        std::uint64_t w;
        in >> u >> v >> w;
        --u, --v;
        edges.emplace_back(&vertices[u], &vertices[v], w, i);
    }
    std::sort(edges.begin(), edges.end(), std::greater<>());
    std::list<Edge> edges_list{edges.begin(), edges.end()};
    remove_mst(edges_list);

    std::uint64_t sum = 0;
    std::vector<bool> used(m, false);
    unsigned used_count = 0;
    for (auto it = edges_list.rbegin(); it != edges_list.rend(); ++it) {
        if (sum + it->w > s) {
            edges_list.erase(edges_list.begin(), it.base());
            break;
        }
        used[it->index] = true;
        ++used_count;
        sum += it->w;
    }

    out << used_count << std::endl;
    for (size_t i = 0; i < m; ++i) {
        if (used[i]) {
            out << i + 1 << " ";
        }
    }
    out << std::endl;
}

int main() {
    std::ifstream in("destroy.in");
    std::ofstream out("destroy.out");
    solve(in, out);
    // solve(std::cin, std::cout);
}
